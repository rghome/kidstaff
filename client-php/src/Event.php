<?php


namespace RM;


use ElephantIO\Client as Elephant;

/**
 * Class Event
 * @package RM
 */
class Event extends Service
{
    /**
     * @var array
     */
    public $types = [
        'system' => 'system'
    ];

    /**
     * @param $data
     *
     * @return string
     */
    public function createSystemEvent($data)
    {
        $ended = (isset($data['ended']) ? true : false);
        $create = date('Y-m-d H:i:s');
        $dataValue = json_encode([
            'name' => $data['name'],
            'text' => $data['text']
        ]);

        $dql = "insert into events(";
        $dql .= "type, emiter_id, data,";
        $dql .= ($ended ? "ended," : "");
        $dql .= "created) values (:type, :emiter, :data,";
        $dql .= ($ended ? ":ended," : "");
        $dql .= ":created);";

        $pdo = $this->getPDO();
        $stmt = $pdo->prepare($dql);

        $stmt->bindParam(':type', $this->types['system'], \PDO::PARAM_STR);
        $stmt->bindParam(':emiter', $_SESSION['user']['id'], \PDO::PARAM_INT);
        $stmt->bindParam(':data', $dataValue, \PDO::PARAM_STR);
        if ($ended) {
            $ended = date('Y-m-d H:i:s', strtotime($data['ended_date']));
            $stmt->bindParam(':ended', $ended, \PDO::PARAM_STR);
        }
        $stmt->bindParam(':created', $create, \PDO::PARAM_STR);
        $stmt->execute();

        return $pdo->lastInsertId();
    }
}