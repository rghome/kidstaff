<?php


namespace RM;

use Interop\Container\ContainerInterface;

class Service
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return \PDO
     */
    protected function getPDO()
    {
        return $this->container->get('db');
    }
}