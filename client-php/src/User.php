<?php


namespace RM;

use \RKA\Session;

/**
 * Class User
 * @package RM
 */
class User extends Service
{
    /**
     * @param array $data
     *
     * @return bool|mixed
     */
    public function login(array $data)
    {
        $pdo = $this->getPDO();

        $dql = "select * from users where `login` = :login and `password` = :password";
        $statement = $pdo->prepare($dql);
        $statement->execute($data);

        if ($user = $statement->fetch()) {
            $session = new Session();
            $session->set('user', $user);

            return $user;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool|mixed
     */
    public function getUserById($id)
    {
        $pdo = $this->getPDO();

        $dql = "select * from users where `id` = :id";
        $statement = $pdo->prepare($dql);
        $statement->execute(['id' => $id]);

        if ($user = $statement->fetch()) {
            return $user;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $pdo = $this->getPDO();

        $dql = "select * from users";
        $statement = $pdo->prepare($dql);
        $statement->execute();

        return $statement->fetchAll();
    }
}