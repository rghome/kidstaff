<?php

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = true;
$config['determineRouteBeforeAppMiddleware'] = true;

date_default_timezone_set('Europe/Kiev');

$config['db']['host']   = "localhost";
$config['db']['user']   = "root";
$config['db']['pass']   = "";
$config['db']['name']   = "kidstaff";
$config['ws']['host']   = "localhost";
$config['ws']['port']   = "3032";