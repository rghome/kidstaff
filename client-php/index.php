<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Interop\Container\ContainerInterface;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\Cookie;
use Dflydev\FigCookies\SetCookie;
use ElephantIO\Engine\SocketIO\Version1X;
use \RKA\Session;
use RM\User;
use RM\Event;

require 'vendor/autoload.php';

$config = [];
require 'config.php';

$app = new \Slim\App(["settings" => $config]);

// Get container
$container = $app->getContainer();

/**
 * @param ContainerInterface $container
 *
 * @return \Slim\Views\Twig
 */
$container['view'] = function (ContainerInterface $container) use ($config) {
    $view = new \Slim\Views\Twig('views');

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->getEnvironment()->addGlobal('session', $_SESSION);
    $view->getEnvironment()->addGlobal('config', $config);

    return $view;
};

/**
 * @param ContainerInterface $c
 *
 * @return PDO
 */
$container['db'] = function (ContainerInterface $c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['name'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
};

/**
 * @param ContainerInterface $container
 *
 * @return \ElephantIO\Client
 */
$container['ws'] = function (ContainerInterface $container) {
    $settings = $container['settings']['ws'];

    $url = 'http://' . $settings['host'] . ':' . $settings['port'] . '/?php_user=' . $_SESSION['user']['id'];
    return new ElephantIO\Client(new Version1X($url));
};

$userService = new User($container);
$eventService = new Event($container);



$app->add(function (Request $request, Response $response, callable $next) use ($userService) {
    $cookies = $request->getCookieParams();

    if (!isset($cookies['user'])) {
        /** @var \Slim\Route $route */
        $route = $request->getAttribute('route');

        if ($route->getName() !== 'login') {
            return $response->withRedirect('/login');
        }
    } else if (isset($cookies['user']) && !isset($_SESSION['user'])) {
        $_SESSION['user'] = $userService->getUserById($cookies['user']);
    }

    return $next($request, $response);
});

$app->add(new \RKA\SessionMiddleware(['name' => 'kidstaff']));

$app->get('/', function (Request $request, Response $response) use ($userService) {
    return $this->view->render($response, 'index.twig', [
        'users' => $userService->getAll()
    ]);
})->setName('index');

$app->get('/login', function (Request $request, Response $response) {
    return $this->view->render($response, 'login.twig');
})->setName('login');

$app->get('/logout', function(Request $request, Response $response) {
    $response = FigResponseCookies::expire($response, 'user');
    unset($_SESSION['user']);

    return $response->withRedirect('/');
})->setName('logout');

$app->post('/login', function(Request $request, Response $response) use ($userService) {
    $data = $request->getParsedBody();

    if ($user = $userService->login($data)) {
        $response = FigResponseCookies::set($response, SetCookie::create('user')
            ->withValue($user['id'])
        );

        return $response->withRedirect('/');
    }

    return $this->view->render($response, 'login.twig', [
        'error' => 'User ' . $data['login'] . ' not found'
    ]);
})->setName('login');

$app->post('/createEvent', function (Request $request, Response $response) use ($eventService, $container) {
    $data = $request->getParsedBody();

    if ($id = $eventService->createSystemEvent($data)) {
        $args = [
            'namespace' => 'event',
            'fn' => 'whenCreatedNewEvent',
            'args' => [
                'eventId' => $id
            ]
        ];

        /** @var \ElephantIO\Client $elephant */
        $elephant = $container['ws'];
        $elephant->initialize();
        $elephant->emit('call', $args);
        $elephant->close();
    }

    return $response->withRedirect('/');
})->setName('createEvent');

$app->run();