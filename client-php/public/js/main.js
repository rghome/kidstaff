/**
 * Created by rg on 01.11.16.
 */

var Main = {
    notify: function (message, options) {
        var settings = options || {};

        settings = jQuery.extend({
            type: 'success',
            delay: 0
        }, settings);

        jQuery(function () {
            jQuery.notify({
                message: message
            }, settings);
        });
    },
    switchEndedTime: function (el) {
        if (jQuery(el).is(':checked')) {
            jQuery('.date-input').show();
        } else {
            jQuery('.date-input').hide();
        }
    }
};

var User = {
    setOnlineUsers: function (users) {
        jQuery('.online-users-cnt').text(Object.keys(users).length);

        jQuery('td i').removeClass('online-doter');
        // jQuery('td a.btn-primary').attr('disabled', true);

        jQuery.each(users, function (key, user) {
            var row = jQuery('tr#row-user-' + key);
            if (row.length) {
                row.find('td:eq(1) i.fa').addClass('online-doter');
                // row.find('a.btn-primary').attr('disabled', false);
            }
        });
    },
    setStatistics: function (data) {
        console.log(data);
    },
    setNotification: function (recipient, module) {
        return socket.emit('call', {namespace: 'event', fn: 'setPrivateEvent', args: {
            recipient: recipient,
            module: module
        }});
    },
    notify: function (ev) {
        var data = JSON.parse(ev.data);

        return Main.notify('Private: ' + ev.module, {
            onClose: function () {
                return EventService.setWatched(ev);
            }
        });
    }
};

var EventService = {
    statisticList: function () {
        return {
            comment: jQuery('.list-group a.notify-counter.comment span p'),
            reply: jQuery('.list-group a.notify-counter.reply span p'),
            message: jQuery('.list-group a.notify-counter.messages span p'),
            system: jQuery('.list-group a.notify-counter.system span p'),
        };
    },
    notify: function (ev) {
        var data = JSON.parse(ev.data);

        return Main.notify('System: ' + data.text, {
            type: 'warning',
            onClose: function () {
                return EventService.setWatched(ev);
            }
        });
    },
    setWatched: function (ev) {
        return socket.emit('call', {namespace: 'event', fn: 'setWatched', args: {eventId: ev.id}}, function () {
            if (ev.type === 'system') {
                EventService.statisticList().system.text(parseInt(EventService.statisticList().system.text()) - 1);
            } else if (ev.type === 'private') {
                EventService.statisticList()[ev.module].text(parseInt(EventService.statisticList()[ev.module].text()) - 1);
            }
        });
    },
    newUpdate: function(ev) {
        if (ev.type === 'system') {
            EventService.statisticList().system.text(parseInt(EventService.statisticList().system.text()) + 1);
            EventService.notify(ev);
        } else if (ev.type === 'private') {
            EventService.statisticList()[ev.module].text(parseInt(EventService.statisticList()[ev.module].text()) + 1);
            User.notify(ev);
        }
    }
};