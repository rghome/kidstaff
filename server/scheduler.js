/**
 * Created by rg on 08.11.16.
 */

var config = require('./config'),
    main = require('./src/libs/main'),
    db = require('./src/libs/mysql/connection')(config.mysql);

var time = require('time');

var now = new time.Date();
now.setTimezone(config.app.timezone);

var datetime = main.toMysqlFormat(now);

var query = 'DELETE e, n ' +
    'FROM events as e ' +
    'LEFT JOIN notifications as n ON (n.event_id = e.id) ' +
    'WHERE e.type = "system" and e.ended <= "' + datetime + '"'
;

db.raw(query).then(function (raw) {
    console.log(raw[0]);

    process.exit(1);
});