/**
 * Created by rg on 31.10.16.
 */
var config = require('./config'),
    io = require('socket.io').listen(config.ws.port),
    App = require('./src/AppFactory'),
    main = require('./src/libs/main');

/**
 * Error handler
 *
 * @param exceptionData
 * @returns {Emitter|Socket|Namespace}
 */
var emitError = function (exceptionData) {
    console.error(exceptionData);
    return io.emit('error', exceptionData);
};

/**
 * Authentication Middleware for PHP Client
 *
 * PHP Client need to set query param named {@var config.app.phpClientCookieName}
 * which holds a User cookie ID.
 */
io.use(function(socket, next) {
    var handshake = socket.handshake;
    var phpClientCookieName = config.app.phpClientCookieName;

    if (handshake.query !== undefined && handshake.query[phpClientCookieName] !== undefined) {
        var date = new Date();
        date.setTime(date.getTime()+(1*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();

        handshake.headers.cookie = config.app.cookieName+"="+handshake.query[phpClientCookieName]+expires+"; path=/";
    }

    next(null, true);
});

/**
 * Client connection event
 */
io.sockets.on('connection', function(socket){
    try {
        /**
         * Initialize application and Services
         *
         * @type {AppFactory}
         */
        var app = new App({
            config: config,
            ws: socket,
            io: io
        });

        var userService = app.getService('userService'),
            eventService = app.getService('eventService');

        /**
         * Setting "local" data
         *
         * @type {{user_id, socket_id: *}}
         */
        socket['ksData'] = {
            user_id: userService.getUserIdFromCookies(),
            socket_id: socket.id
        };

        /**
         * Key-value JS object which store
         * app clients and their sessions
         */
        app.loadStore();

        /**
         * Main emit-listener from client
         *
         * Required params:
         * @namespace – Service Name
         * @fn - Service Method Name
         * @cl – Emitter callback
         * @args – Emitter arguments (data)
         */
        socket.on('call', function(data, cl) {
            var args = undefined;

            if (data === undefined) {
                return emitError(new app.exceptions(204));
            }

            /**
             * Service Name
             */
            if (data['namespace'] === undefined) {
                return emitError(new app.exceptions(202));
            }

            /**
             * Service Function
             */
            if (data['fn'] === undefined) {
                return emitError(new app.exceptions(203));
            }

            /**
             * Callback if need
             */
            if (data['cl'] !== undefined) {
                cl = data['cl'];
            }

            /**
             * Arguments if need
             */
            if (data['args'] !== undefined) {
                args = data['args'];
            }

            if (data.namespace === 'event') {
                eventService[data.fn](args, cl);
            } else if (data.namespace === 'user') {
                userService[data.fn](args, cl);
            }
        });

        io.emit('getOnlineUsers', app.getStore().users);
    } catch (exceptionData) {
        emitError(exceptionData);
    }

    /**
     * Client disconnect event
     */
    socket.on('disconnect', function() {
        if (app !== undefined) {
            var ksData = socket['ksData'];

            /**
             * Remove user active session
             */
            var users = main.removeByValue(
                app.getStore().users[ksData['user_id']],
                ksData['socket_id']
            );

            /**
             * Remove user when his don't have anyone active session
             */
            if (!users.length) {
                delete app.getStore().users[ksData['user_id']];
            }

            /**
             * Remove session id
             */
            delete app.getStore().sessions[ksData['socket_id']];

            io.emit('getOnlineUsers', app.getStore().users);
        }
    });
});