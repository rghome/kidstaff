/**
 * Created by rg on 31.10.16.
 */

module.exports = AppFactory;

/**
 * Storage
 *
 * @type {{users: {}, sessions: {}}}
 */
var dataStore = {
    users: {},
    sessions: {}
};

/**
 *
 * @param config
 * @returns {AppFactory}
 * @constructor
 */
function AppFactory (options) {
    this.config = options.config;
    this.ws = options.ws;
    this.io = options.io;

    this.loadExceptions();
    this.loadDB();
    this.loadServices();

    return this;
}

/**
 * Load Exceptions errors pack
 */
AppFactory.prototype.loadExceptions = function () {
    this.exceptions = require('./AppExceptions');
};

/**
 * Load MySQL driver
 */
AppFactory.prototype.loadDB = function() {
    this.db = require('./libs/mysql/connection')(this.config.mysql);
};

/**
 * Load Services
 *
 * Method read dir ./services and Load all Modules
 * that match the pattern:
 * ```
 * {
 *  name: 'ServiceName',
 *  load: Service // Service constructor
 * }
 * ```
 */
AppFactory.prototype.loadServices = function () {
    this.service = {};

    var self = this;
    var normalizedPath = require('path').join(__dirname, './services');

    require("fs").readdirSync(normalizedPath).forEach(function(file) {
        var service = require("./services/" + file);

        if (service.hasOwnProperty('name')) {
            self.service[service.name] = new service.load({
                instance: self
            });
        }
    });
};

/**
 * Get Service by Name
 *
 * @param name
 * @returns {*}
 */
AppFactory.prototype.getService = function (name) {
    if (!this.service.hasOwnProperty(name)) {
        throw new this.exceptions(201);
    }

    return this.service[name];
};

/**
 * Load store with user data
 */
AppFactory.prototype.loadStore = function () {
    var ksData = this.ws['ksData'];

    if (dataStore.users[ksData.user_id] === undefined) {
        dataStore.users[ksData.user_id] = [];
    }

    dataStore.users[ksData.user_id].push(ksData.socket_id);
    dataStore.sessions[ksData.socket_id] = ksData.user_id;
};

/**
 * Get store Storage
 *
 * @returns {{users: {}, sessions: {}}}
 */
AppFactory.prototype.getStore = function () {
    return dataStore;
};