/**
 * Created by rg on 31.10.16.
 */

module.exports = AppExceptions;

function AppExceptions(id) {
    var exception = this.loadData()[id];

    return exception;
};

AppExceptions.prototype.loadData = function() {
    var types = {
        system: 'System',
        user: 'User Module'
    };

    return {
        // System Errors
        201: {
            type: types.system,
            message: "Can't load service."
        },
        202: {
            type: types.system,
            message: "Namespace is undefined"
        },
        203: {
            type: types.system,
            message: "Function is undefined"
        },
        204: {
            type: types.system,
            message: "Arguments not sufficiently"
        },

        // Users Errors
        301: {
            type: types.user,
            message: "Cookie not found."
        },
        302: {
            type: types.user,
            message: "User not found."
        }
    }
};