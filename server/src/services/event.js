/**
 * Created by rg on 07.11.16.
 */

var main = require('../libs/main');
var time = require('time');

/**
 * Export service and meta-data
 *
 * @type {{name: string, load: EventService}}
 */
module.exports = {
    name: 'eventService',
    load: EventService
};

/**
 * Init EventService
 *
 * @param options
 * @returns {EventService}
 * @constructor
 */
function EventService(options) {
    this.app = options.instance;
    return this;
}

EventService.prototype.setPrivateEvent = function (args) {
    return this.setEvent(args, 'private');
};

EventService.prototype.setSystemEvent = function (args) {
    return this.setEvent(args, 'system');
};

/**
 * Create new event
 *
 * @param args
 * @param type
 */
EventService.prototype.setEvent = function (args, type) {
    var $this = this;
    var now = new time.Date();
    now.setTimezone(this.app.config.app.timezone);

    var datetime = main.toMysqlFormat(now);

    var data = {
        type: type,
        emiter_id: parseInt(this.app.ws.ksData.user_id),
        recipient_id: (args.recipient !== undefined ? parseInt(args.recipient) : null),
        module: (args.module !== undefined ? args.module : null),
        data: (args.data !== undefined ? JSON.stringify(args.data) : null),
        created: datetime
    };

    var a = this.app.db
        .table('events')
        .insert(data)
        .then(function (id) {
            $this.whenCreatedNewEvent({eventId: id});
        })
        .catch(function (e) {
            console.log(e);
        })
    ;
};

/**
 * When Client created new event
 * need to call this method to notify users
 *
 * @param data
 * @returns {*}
 */
EventService.prototype.whenCreatedNewEvent = function (data) {
    var $this = this;

    var eventId = data.eventId;
    return this.getEventById(eventId)
        .then(function (row) {
            if (!row.length) {
                throw new Error('Event with id: ' + eventId + ' not found in DB');
            }

            return row[0];
        })
        .then(function (row) {
            if (row['type'] === 'system') {
                return $this.emitSystemEvent(row);
            } else if (row['type'] === 'private') {
                return $this.emitPrivateEvent(row);
            }
        }, function (err) {
            console.error(err.message);
        })
    ;
};

/**
 * Notify all online users about a new system event
 *
 * @param ev
 */
EventService.prototype.emitSystemEvent = function (ev) {
    var sessions = this.app.getStore()['sessions'];
    for (var index in sessions) {
        // @todo: fix this.app.ws.ksData.socket_id !== index
        if (sessions.hasOwnProperty(index) && this.app.ws.ksData.socket_id !== index) {
            this.app.io.sockets.in(index).emit('getSystemEvent', ev);
        }
    }
};

/**
 * Notify Recipient online user about a new private event
 *
 * @param ev
 */
EventService.prototype.emitPrivateEvent = function (ev) {
    var $this = this;
    var user = this.app.getStore()['users'][ev['recipient_id']];
    if (user !== undefined) {
        user.forEach(function (session) {
            $this.app.io.sockets.in(session).emit('getPrivateEvent', ev);
        });
    }
};

/**
 * Get Event by ID from DB
 *
 * @param id
 * @returns {*}
 */
EventService.prototype.getEventById = function(id) {
    return this.app.db
        .select()
        .table('events')
        .where('id', id)
    ;
};

/**
 * Marks a notice of review
 *
 * @param data
 * @returns {*}
 */
EventService.prototype.setWatched = function (data, cl) {
    var $this = this;
    var eventId = data.eventId;

    $this.getEventById(eventId)
        .then(function (row) {
            if (row[0].type === 'system') {
                return $this.addNotification(row[0].id);
            } else if (row[0].type === 'private') {
                return $this.removeEvent(row[0].id);
            }
        })
    ;

    cl(true);
};

/**
 * Remove event
 *
 * @param eventId
 * @returns {*}
 */
EventService.prototype.removeEvent = function (eventId) {
    var $this = this;

    return this.app.db
        .table('events')
        .where('id', eventId)
        .delete()
    ;
};

/**
 * Add new Notification
 *
 * @param eventId
 * @returns {*}
 */
EventService.prototype.addNotification = function (eventId) {
    var $this = this;

    return this.app.db
        .insert({
            event_id: eventId,
            user_id: parseInt($this.app.ws.ksData.user_id)
        }, 'id')
        .into('notifications')
    ;
};

/**
 * Get all unwatched notifications for a user
 * and send to all active sessions
 *
 * @param data
 * @param cb
 * @returns {*}
 */
EventService.prototype.getUpdates = function (data, cb) {
    var $this = this;
    var ksData = $this.app.ws.ksData;
    var users = $this.app.getStore()['users'];

    // SELECT e.*
    // FROM `events` as `e`
    // LEFT JOIN `notification` as `n` ON (`n`.`event_id` = `e`.`id`)
    // WHERE (`e`.`type` = 'system' OR `e`.`recipient_id` = 1) AND `n`.`id` IS NULL;

    return this.app.db
        .select('events.*')
        .from('events')
        .leftJoin('notifications', function () {
            this.on('notifications.event_id', '=', 'events.id')
                .andOn('notifications.user_id', '=', parseInt(ksData.user_id));
        })
        .where(function () {
            this.where('events.type', 'system')
                .orWhere('events.recipient_id', parseInt(ksData.user_id));
        })
        .andWhere(function () {
            this.whereNull('notifications.id');
        })
        .then(function (rows) {
            if (users[ksData.user_id].length) {
                return users[ksData.user_id].forEach(function (session) {
                    $this.app.io.sockets.in(session).emit('newUpdates', rows);
                });
            }
        })
    ;
};