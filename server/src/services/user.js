/**
 * Created by rg on 31.10.16.
 */

var main = require('../libs/main');

/**
 * Export service and meta-data
 *
 * @type {{name: string, load: User}}
 */
module.exports = {
    name: 'userService',
    load: User
};

/**
 * Init UserService
 *
 * @param options
 * @returns {User}
 * @constructor
 */
function User(options) {
    /**
     * Create copy of factory
     */
    this.app = options.instance;
    return this;
}

/**
 * Get User Id from cookies for auth
 *
 * @returns {*}
 */
User.prototype.getUserIdFromCookies = function () {
    var self = this;

    var cookie = main.parseCookies(this.app.ws.request),
        cookieName = this.app.config.app.cookieName;

    if (!cookie.hasOwnProperty(cookieName)) {
        throw new this.app.exceptions(301);
    }

    return cookie[cookieName];
};

/**
 * Get current User from DB
 *
 * @returns {*}
 */
User.prototype.setSessionUser = function() {
    return this.getUserById(this.getUserIdFromCookies())
        .then(function (row) {
            self.session = row[0];

            return self;
        })
    ;
};

/**
 * Get User by ID from DB
 *
 * @param id
 * @returns {*}
 */
User.prototype.getUserById = function(id) {
    return this.app.db
        .select()
        .table('users')
        .where('id', id)
    ;
};