/**
 * Created by rg on 31.10.16.
 */

/**
 * @param options
 * @returns {*}
 */
module.exports = function (options) {
    return require('knex')({
        client: 'mysql',
        connection: options
    });
};