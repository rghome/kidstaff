/**
 * Created by rg on 31.10.16.
 */

module.exports = {
    parseCookies: function(request) {
        var list = {},
            rc = request.headers.cookie;

        rc && rc.split(';').forEach(function( cookie ) {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });

        return list;
    },
    removeByValue: function (array, val) {
        if (array !== undefined) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] === val) {
                    array.splice(i, 1);
                    i--;
                }
            }
            return array;
        }
    },
    twoDigits: function (d) {
        if(0 <= d && d < 10) return "0" + d.toString();
        if(-10 < d && d < 0) return "-0" + (-1*d).toString();
        return d.toString();
    },
    toMysqlFormat: function(date) {
        return date.getFullYear()
            + "-" + this.twoDigits(1 + date.getMonth())
            + "-" + this.twoDigits(date.getDate())
            + " " + this.twoDigits(date.getHours())
            + ":" + this.twoDigits(date.getMinutes())
            + ":" + this.twoDigits(date.getSeconds())
        ;
    }
};