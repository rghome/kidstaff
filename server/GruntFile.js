/**
 * Created by rg on 29.10.16.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        // jshint : {
        //     myFiles : ['./Server/<strong>/*.js','./Routes/</strong>/*.js']
        // },
        jshint: {
            myFiles: ['./index.js', './src/*.js', './src/<strong>/*.js']
        },
        nodemon : {
            script : './index.js',
            "ext": "js html"
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.registerTask('default', ['jshint','nodemon']);
};