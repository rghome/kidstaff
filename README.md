Требования
-------
 - npm
 - nodejs
 - mysql
 - apache
 - php7
 - composer
 - curl
 - php-mbstring 
 - git 
 - unzip
 - bower
 - supervisor

Установка
-------

#### Clone repository ####
    cd /var/www
    git clone https://rghome@bitbucket.org/rghome/kidstaff.git

#### Install php components ####
    /var/www/kidstaff/client-php$ sudo composer install

#### Install bower components ####

    /var/www/kidstaff/client-php$ bower install

#### Install npm components ####

	/var/www/kidstaff/server$ sudo npm install

Настройка
-------
#### Конфиг для node-сервера ####

    /var/www/kidstaff/server$ cp config.sample.json config.json
Необходимо указать данные для подключения к БД в секции `mysql`:
		    
	"mysql": {
        "host" : "127.0.0.1",
	    "user" : "root",
	    "password" : null,
	    "database" : "kidstaff"
	},

#### Конфиг для php-клиента ####
    /var/www/kidstaff/php-client$ cp config.sample.php config.php
Необходимо указать данные для подключения к БД и WebSocket в файле `config.php`:

	$config['db']['host']   = "127.0.0.1";
	$config['db']['user']   = "root";
	$config['db']['pass']   = "";
	$config['db']['name']   = "kidstaff";
	$config['ws']['host']   = "89.184.82.43"; // servername host
	$config['ws']['port']   = "3032";

#### Импорт дампа базы ####

    /var/www/kidstaff$ mysql -u root -pPWD kidstaff < dump.sql